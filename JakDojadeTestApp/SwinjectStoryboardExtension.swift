//
//  SwinjectStoryboardExtension.swift
//  JakDojadeTestApp
//
//  Created by Bartłomiej Smektała on 08/11/2020.
//

import Swinject
import SwinjectStoryboard
import AppInjection

extension SwinjectStoryboard {
    @objc class func setup() {
        _ = ViewControllerContainer(defaultContainer: defaultContainer)
    }
}
